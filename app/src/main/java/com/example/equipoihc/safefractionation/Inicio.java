package com.example.equipoihc.safefractionation;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Connection;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Inicio extends AppCompatActivity {
    //Variables de Objetos
    static TextView usuario;
    static TextView contra;
    EditText pass;
    Button iniciar;
    Button nuevoregistro;
    Button ayuda;

    //Base de datos
    RequestQueue requestQueue;
    //String cuenta="equipoihc123";
    //String password="1234";
    Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        //Identificadores (id) con relacion al objeto
        usuario=(TextView)findViewById(R.id.Cuenta);
        pass=(EditText)findViewById(R.id.Pass);
        iniciar=(Button) findViewById(R.id.IniciarSesion);
        nuevoregistro=(Button)findViewById(R.id.NuevoRegistro);
        ayuda=(Button)findViewById(R.id.Ayuda);



        //Accion al intentar acceder a la aplicacion
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();

            }
        });

        nuevoregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg=new Intent(view.getContext(), Registro.class);
                startActivityForResult(reg,0);
            }
        });
        ayuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dato=new AlertDialog.Builder(Inicio.this);
                dato.setTitle("Consejo");
                dato.setMessage("Las ventanas: Residentes, Visitantes, Agregar Nota y Editar Nota se pueden girar para mayor comodidad.");
                dato.setPositiveButton("Entendido", null);
                dato.show();
            }
        });
    }

    public void ingresar(){
        String variableUsuario;
        String variablePass;

        SQLite_OpenHelper helper=new SQLite_OpenHelper(this,"safefrac",null,1);
        SQLiteDatabase db=helper.getReadableDatabase();

        variableUsuario=usuario.getText().toString();
        variablePass=pass.getText().toString();
        fila=db.rawQuery("select*from usuario where nombre='"+variableUsuario+"' and contrasena='"+variablePass+"'",null);

        if(fila.moveToFirst()==true){
            String us=fila.getString(0);
            String pa=fila.getString(1);
            if(variableUsuario.equals(us)  && variablePass.equals(pa)){
                //Dialogo de alerta de acceso o de error
                Toast.makeText(Inicio.this, "Bienvenido "+variableUsuario, Toast.LENGTH_SHORT).show();

            }
            else{
                AlertDialog.Builder denegado=new AlertDialog.Builder(Inicio.this);
                denegado.setMessage("Usuario y/o contraseña incorrectos");
                denegado.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        usuario.setText("");
                        pass.setText("");
                    }
                });
                denegado.show();
            }
        }
        else{
            Toast.makeText(Inicio.this, "Bienvenido "+variableUsuario, Toast.LENGTH_SHORT).show();
            Intent ven=new Intent(this,Menu.class);
            startActivity(ven);
        }

    }

    /*
    public java.sql.Connection conexionBD(){
        java.sql.Connection conexion=null;
        try{
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conexion= DriverManager.getConnection("jdbc:jtds:sqlserver://10.127.127.1:1433;databaseName=safefractionation;user=sa;password=e1657a815");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        return conexion;
    }

    public void ingresaUsuario() {
        String nom=null;
        String cont=null;
        try{
            PreparedStatement ps=conexionBD().prepareStatement("SELECT * FROM usuarios WHERE nombre='"+nom+"' AND contrasena'"+cont+"'");
            ps.setString(0, nom);
            ps.setString(1, cont);
            ResultSet rs=ps.executeQuery();
        }
        catch(SQLException e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
    */
}