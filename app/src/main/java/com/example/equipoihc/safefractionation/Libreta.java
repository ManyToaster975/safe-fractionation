package com.example.equipoihc.safefractionation;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Libreta extends AppCompatActivity {

    ImageButton menu,agregar,eliminar,editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libreta);
        menu=(ImageButton)findViewById(R.id.Menu);
        agregar=(ImageButton)findViewById(R.id.Agregar);
        editar=(ImageButton)findViewById(R.id.Editar);
        eliminar=(ImageButton)findViewById(R.id.Eliminar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ad=new Intent(view.getContext(), AgregarNota.class);
                startActivityForResult(ad,0);
            }
        });
        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ed=new Intent(view.getContext(), EditarNota.class);
                startActivityForResult(ed,0);
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent e=new Intent(view.getContext(),BorrarHoja.class);
                startActivityForResult(e,0);
            }
        });
    }
}
