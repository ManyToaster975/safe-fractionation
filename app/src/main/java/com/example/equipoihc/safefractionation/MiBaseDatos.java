package com.example.equipoihc.safefractionation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emanuel Garcia on 03/12/2017.
 */

public class MiBaseDatos extends SQLiteOpenHelper {

    private static final int VERSION_BASEDATOS = 1;

    // Nombre de nuestro archivo de base de datos
    private static final String NOMBRE_BASEDATOS = "mibasedatos.db";

    // Sentencia SQL para la creación de una tabla
    private static final String TABLA_CONTACTOS = "CREATE TABLE visitantes" +
            "(_id INT PRIMARY KEY AUTOINCREMENT, nombre TEXT, direccion TEXT, identificacion TEXT, placas TEXT NULL, entradahora TEXT, entradadia TEXT, salidahora TEXT NULL, salidadia TEXT NULL)";


    // CONSTRUCTOR de la clase
    public MiBaseDatos(Context context) {
        super(context, NOMBRE_BASEDATOS, null, VERSION_BASEDATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_CONTACTOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CONTACTOS);
        onCreate(db);
    }


    public void modificarCONTACTO(String nom,String sh,String sd){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("nombre", nom);
        valores.put("salidahora", sh);
        valores.put("salidadia", sd);
        db.update("visitantes", valores, "nombre=" + nom, null);
        db.close();
    }

    public List<Contactos> recuperarCONTACTOS() {
        SQLiteDatabase db = getReadableDatabase();
        List<Contactos> lista_contactos = new ArrayList<Contactos>();
        String[] valores_recuperar = {"nombre", "direccion", "identificacion", "placas","entradahora","entradadia","salidahora","salidadia"};
        Cursor c = db.query("visitantes", valores_recuperar,
                null, null, null, null, null, null);
        c.moveToFirst();
        do {
            Contactos contactos = new Contactos(c.getString(0), c.getString(1), c.getString(2), c.getString(3),c.getString(4), c.getString(5), c.getString(6), c.getString(7));
            lista_contactos.add(contactos);
        } while (c.moveToNext());
        db.close();
        c.close();
        return lista_contactos;
    }


    public void insertarCONTACTO(String s, String s1, String s2, String s3, String s4, String s5, String s6, String s7) {
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            ContentValues valores = new ContentValues();
            valores.put("nombre", s);
            valores.put("direccion", s1);
            valores.put("identificacion", s2);
            valores.put("placas", s3);
            valores.put("entradahora", s4);
            valores.put("entradadia", s5);
            valores.put("salidahora", s6);
            valores.put("salidadia", s7);
            db.insert("contactos", null, valores);
            db.close();
        }
    }
}