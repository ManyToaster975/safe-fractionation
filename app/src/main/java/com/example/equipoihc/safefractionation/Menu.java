package com.example.equipoihc.safefractionation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Menu extends AppCompatActivity {

    ImageButton n,v,r,reg,p;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        n=(ImageButton)findViewById(R.id.Notas);
        v=(ImageButton)findViewById(R.id.VS);
        r=(ImageButton)findViewById(R.id.Residente);
        reg=(ImageButton)findViewById(R.id.RegistroDatos);
        p=(ImageButton)findViewById(R.id.Perfil);

        n.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent lib=new Intent(view.getContext(), Libreta.class);
                startActivityForResult(lib,0);
            }
        });

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent vis=new Intent(view.getContext(), Visita.class);
                startActivityForResult(vis,0);
            }
        });
        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resident=new Intent(view.getContext(), Residentes.class);
                startActivityForResult(resident,0);
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regis=new Intent(view.getContext(), RegistroDatos.class);
                startActivityForResult(regis,0);
            }
        });
        p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pp=new Intent(view.getContext(), Perfil.class);
                startActivityForResult(pp,0);
            }
        });
    }
}
