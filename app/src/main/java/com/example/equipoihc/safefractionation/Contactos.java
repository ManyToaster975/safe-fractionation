package com.example.equipoihc.safefractionation;

/**
 * Created by Emanuel Garcia on 03/12/2017.
 */

public class Contactos {

    private String nombre;
    private String direccion;
    private String identificacion;
    private String placas;
    private String entradahora;
    private String entradadia;
    private String salidahora;
    private String salidadia;

    // Constructor de un objeto Contactos
    public Contactos(String nombre, String direccion, String identificacion,String placas, String entradahora, String entradadia,String salidahora, String salidadia) {
        this.setNombre(nombre);
        this.setDireccion(direccion);
        this.setIdentificacion(identificacion);
        this.setPlacas(placas);
        this.setEntradahora(entradahora);
        this.setEntradadia(entradadia);
        this.setSalidahora(salidahora);
        this.setSalidadia(salidadia);
    }

    // Recuperar/establecer NOMBRE
    public String getNOMBRE() {
        return getNombre();
    }
    public void setNOMBRE(String nombre) {
        this.setNombre(nombre);
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getEntradahora() {
        return entradahora;
    }

    public void setEntradahora(String entradahora) {
        this.entradahora = entradahora;
    }

    public String getEntradadia() {
        return entradadia;
    }

    public void setEntradadia(String entradadia) {
        this.entradadia = entradadia;
    }

    public String getSalidahora() {
        return salidahora;
    }

    public void setSalidahora(String salidahora) {
        this.salidahora = salidahora;
    }

    public String getSalidadia() {
        return salidadia;
    }

    public void setSalidadia(String salidadia) {
        this.salidadia = salidadia;
    }
}
