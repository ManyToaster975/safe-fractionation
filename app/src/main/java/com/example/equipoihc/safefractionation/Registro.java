package com.example.equipoihc.safefractionation;

import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Registro extends AppCompatActivity {

    private final static String[] edades= new String[]{"00","20","21","22","23","24","25","26","27","28","29","30","31","32","33",
            "34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54",
            "55","56","57","58","59","60","61","62","63","64","65"};
    Button registrar;
    EditText nombre;
    EditText correo;
    RadioButton hombre;
    RadioButton mujer;
    EditText contra;
    Spinner edad;
    String dato1;
    String dato2;
    String dato3;

    String dato4;
    String dato5;
    String dato6;

    final SQLite_OpenHelper helper=new SQLite_OpenHelper(this,"safefrac",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        registrar=(Button)findViewById(R.id.MasUsuario);
        nombre=(EditText)findViewById(R.id.Nombre);
        correo=(EditText)findViewById(R.id.Correo);
        hombre=(RadioButton) findViewById(R.id.Hombre);
        mujer=(RadioButton)findViewById(R.id.Mujer);
        contra=(EditText)findViewById(R.id.Contraseña);
        edad=(Spinner) findViewById(R.id.Edad);

        //Datos del Spinner
        ArrayAdapter ed=new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, edades);
        edad.setAdapter(ed);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dato1=nombre.getText().toString();
                dato2=correo.getText().toString();
                dato3=hombre.getText().toString();
                dato4=mujer.getText().toString();
                dato5=edad.getSelectedItem().toString();
                dato6=contra.getText().toString();
                if(dato1.equals("") || dato2.equals("") || dato3.equals("")){
                    AlertDialog.Builder falta=new AlertDialog.Builder(Registro.this);
                    falta.setMessage("Error de datos, favor de corregir o completar los requeridos");
                    falta.setPositiveButton("OK", null);
                    falta.show();
                }
                else{
                    helper.abrir();
                    helper.insertReg(dato1,dato2,dato3,dato4,dato5,dato6);
                    Toast.makeText(Registro.this, "El usuario ya ha sido registrado!", Toast.LENGTH_SHORT).show();
                    helper.cerrar();
                    finish();
                }
            }
        });

        //Hace una accion al seleccionar Masculino
        hombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hombre.setText("Masculino");
                mujer.setText("NA");
            }
        });

        //Hace una accion al seleccionar Femenino
        mujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hombre.setText("NA");
                mujer.setText("Femenino");
            }
        });
    }
/*
    public java.sql.Connection conexionBD(){
        java.sql.Connection conexion=null;
        try{
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conexion= DriverManager.getConnection("jdbc:jtds:sqlserver://10.127.127.1:1433;databaseName=safefractionation;user=sa;password=e1657a815");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        return conexion;
    }

    public void agregarUsuario(){
        try{
            PreparedStatement pst=conexionBD().prepareStatement("insert into usuarios values(?,?,?,?,?,?)");
            pst.setString(1,nombre.getText().toString());
            pst.setString(2,correo.getText().toString());
            pst.setString(3,hombre.getText().toString());
            pst.setString(4,mujer.getText().toString());
            pst.setString(5,edad.getSelectedItem().toString());
            pst.setString(6,contra.getText().toString());
            pst.executeUpdate();
        }
        catch(SQLException e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
    */
}
