package com.example.equipoihc.safefractionation;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
public class BorrarHoja extends AppCompatActivity {

    Button delete;
    Spinner list;
    EditText b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrar_hoja);
        delete=(Button)findViewById(R.id.Borrar);
        b=(EditText)findViewById(R.id.NombreArchivo);

        final List<String> item = new ArrayList<String>();
        final File route=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Notas");
        final File[] files = route.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.isDirectory()){
                item.add(file.getName() + "/");
            }
            else{
                item.add(file.getName());
            }
        }


        b.setEnabled(false);

        list = (Spinner) findViewById(R.id.ListaArchivos);
        final ArrayAdapter<String> fileList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, item);
        list.setAdapter(fileList);
        list.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                b.setText(list.getItemAtPosition(i).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final File dato=new File(b.getText().toString());
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder borro=new AlertDialog.Builder(BorrarHoja.this);
                borro.setTitle("AVISO");
                borro.setMessage("Esta seguro de eliminar esta nota?");
                borro.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dato.isFile()){
                            dato.delete();
                            Toast.makeText(BorrarHoja.this,"La nota ha sido eliminada",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else{
                            Toast.makeText(BorrarHoja.this,"La nota no ha podido ser eliminada",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                borro.setNegativeButton("No",null);
                borro.show();
            }
        });
    }
}
