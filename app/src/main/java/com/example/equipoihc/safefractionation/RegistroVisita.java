package com.example.equipoihc.safefractionation;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class RegistroVisita extends AppCompatActivity {

    EditText nombre2,direccion,identificacion, placa;
    Button registro;
    TextView f,h;
    ImageButton menu;

    //MiBaseDatos MBD=new MiBaseDatos(getApplicationContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_visita);


        //Obtiene la fecha
        final Date d=new Date();
        f=(TextView)findViewById(R.id.fecha);
        SimpleDateFormat fec=new SimpleDateFormat("d'/'MMMM'/'yyyy");
        String fechaCompleta=fec.format(d);
        f.setText(fechaCompleta);

        //Obtiene hora
        h=(TextView)findViewById(R.id.hora);
        SimpleDateFormat hor=new SimpleDateFormat("h:mm a");
        String ho=hor.format(d);
        h.setText(ho);

        registro=(Button)findViewById(R.id.Registrar);
        nombre2=(EditText) findViewById(R.id.Nombre);
        direccion=(EditText) findViewById(R.id.Direccion);
        identificacion=(EditText)findViewById(R.id.Identifiacion);
        menu=(ImageButton)findViewById(R.id.Menu);
        placa=(EditText)findViewById(R.id.Placas);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menu=new Intent(view.getContext(), Menu.class);
                startActivityForResult(menu,0);
            }
        });
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombre2.getText().toString().isEmpty() || direccion.getText().toString().isEmpty() || identificacion.getText().toString().isEmpty()){
                    AlertDialog.Builder falta=new AlertDialog.Builder(RegistroVisita.this);
                    falta.setMessage("Error de datos, favor de corregir o completar los requeridos");
                    falta.setPositiveButton("OK", null);
                    falta.show();
                }
                else{
                    //MBD.insertarCONTACTO(nombre2.getText().toString(),direccion.getText().toString(),identificacion.getText().toString(),placa.getText().toString(),h.getText().toString(),f.getText().toString(),h.getText().toString(),f.getText().toString());
                    //agregarVisitante();
                    Toast.makeText(RegistroVisita.this, "El residente ya ha sido registrado!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }



    /*
    public java.sql.Connection conexionBD(){
        java.sql.Connection conexion=null;
        try{
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conexion= DriverManager.getConnection("jdbc:jtds:sqlserver://10.127.127.1:1433;databaseName=safefractionation;user=sa;password=e1657a815");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        return conexion;
    }

    public void agregarVisitante(){
        try{
            PreparedStatement pst=conexionBD().prepareStatement("insert into visitantes values(?,?,?,?,?,?)");
            pst.setString(1,nombre2.getText().toString());
            pst.setString(2,direccion.getText().toString());
            pst.setString(3,identificacion.getText().toString());
            pst.setString(4,placa.getText().toString());
            pst.setString(5,f.getText().toString());
            pst.setString(6,h.getText().toString());
            pst.executeUpdate();
        }
        catch(SQLException e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
    */
}
