package com.example.equipoihc.safefractionation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class RegistroDatos extends AppCompatActivity {

    ImageButton rr,rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_datos);
        rr=(ImageButton)findViewById(R.id.RegistroResidente);
        rv=(ImageButton)findViewById(R.id.RegistroVisitante);

        rr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent rres=new Intent(view.getContext(),RegistroResidentes.class);
                startActivityForResult(rres,0);
            }
        });
        rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent rvis=new Intent(view.getContext(),RegistroVisita.class);
                startActivityForResult(rvis,0);
            }
        });
    }
}
