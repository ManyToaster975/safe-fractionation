package com.example.equipoihc.safefractionation;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class EditarNota extends AppCompatActivity {

    Button guardar,ver;
    EditText contenido,nombrenota;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_nota);
        guardar=(Button)findViewById(R.id.Guardar);
        ver=(Button)findViewById(R.id.Ver);
        contenido=(EditText)findViewById(R.id.Contenido);
        nombrenota=(EditText)findViewById(R.id.NombreA);
        ver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombrenota.getText().toString().isEmpty()){
                    Toast.makeText(EditarNota.this,"Favor de ingresar el nombre del archivo",Toast.LENGTH_SHORT).show();
                }
                else{
                    try {
                        File route=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Notas");
                        File f=new File(route.getAbsolutePath(),nombrenota.getText().toString());
                        BufferedReader carga=new BufferedReader(new InputStreamReader((new FileInputStream(f))));
                        String cont=carga.readLine();
                        contenido.setText(cont);
                        carga.close();
                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    File route=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Notas");
                    if(route.mkdirs()){
                        File f=new File(route.getAbsolutePath(),nombrenota.getText().toString());
                        OutputStreamWriter hoja=new OutputStreamWriter(new FileOutputStream(f));
                        hoja.write(contenido.getText().toString());
                        hoja.close();
                        Toast.makeText(EditarNota.this, "El archivo se ha actualizado!", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        File f=new File(route.getAbsolutePath(),nombrenota.getText().toString());
                        OutputStreamWriter hoja=new OutputStreamWriter(new FileOutputStream(f));
                        hoja.write(contenido.getText().toString());
                        hoja.close();
                        Toast.makeText(EditarNota.this, "El archivo se ha actualizado!", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception ex){
                    Log.e("Datos","Error al escribir en hoja en memoria Externa");
                }
                finally {
                    finish();
                }
            }
        });
    }
}
