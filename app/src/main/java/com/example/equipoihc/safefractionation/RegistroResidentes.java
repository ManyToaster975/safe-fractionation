package com.example.equipoihc.safefractionation;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegistroResidentes extends AppCompatActivity {

    EditText nombre2,direccion,telefono;
    Button registro;
    ImageButton menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_residentes);
        registro=(Button)findViewById(R.id.Registrar);
        nombre2=(EditText) findViewById(R.id.Nombre);
        direccion=(EditText) findViewById(R.id.Direccion);
        telefono=(EditText)findViewById(R.id.Telefono);
        menu=(ImageButton)findViewById(R.id.Menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menu=new Intent(view.getContext(), Menu.class);
                startActivityForResult(menu,0);
            }
        });

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombre2.getText().toString().isEmpty() || direccion.getText().toString().isEmpty() || telefono.getText().toString().isEmpty()){
                    AlertDialog.Builder falta=new AlertDialog.Builder(RegistroResidentes.this);
                    falta.setMessage("Error de datos, favor de corregir o completar los requeridos");
                    falta.setPositiveButton("OK", null);
                    falta.show();
                }
                else{
                    agregarResidente();
                    Toast.makeText(RegistroResidentes.this, "El residente ya ha sido registrado!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    public java.sql.Connection conexionBD(){
        java.sql.Connection conexion=null;
        try{
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conexion= DriverManager.getConnection("jdbc:jtds:sqlserver://10.127.127.1:1433;databaseName=safefractionation;user=sa;password=e1657a815");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        return conexion;
    }

    public void agregarResidente(){
        try{
            PreparedStatement pst=conexionBD().prepareStatement("insert into residentes values(?,?,?)");
            pst.setString(1,nombre2.getText().toString());
            pst.setString(2,direccion.getText().toString());
            pst.setString(3,telefono.getText().toString());
            pst.executeUpdate();
        }
        catch(SQLException e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}
