package com.example.equipoihc.safefractionation;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Perfil extends AppCompatActivity {

    Button adios;
    EditText nombre,numero,correo,edad,genero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        adios=(Button)findViewById(R.id.Goodbye);
        numero=(EditText)findViewById(R.id.Numero);
        nombre=(EditText) findViewById(R.id.Nombre);
        correo=(EditText)findViewById(R.id.Correo);
        edad=(EditText)findViewById(R.id.Edad);
        genero=(EditText)findViewById(R.id.G);

        //Hace que el EditText no se pueda editar
        numero.setEnabled(false);
        nombre.setEnabled(false);
        correo.setEnabled(false);
        edad.setEnabled(false);
        genero.setEnabled(false);

        adios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder falta=new AlertDialog.Builder(Perfil.this);
                falta.setMessage("Esta seguro de cerrar sesion?");
                falta.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(getBaseContext(),Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        Toast.makeText(Perfil.this, "Nos vemos!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                falta.setNegativeButton("No",null);
                falta.show();
            }
        });
    }
}
