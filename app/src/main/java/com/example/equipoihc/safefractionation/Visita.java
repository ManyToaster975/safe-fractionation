package com.example.equipoihc.safefractionation;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Visita extends AppCompatActivity {

    ImageButton menu,buscar;
    Button salir;
    String[]bus={"Nombre","Direccion","Identificacion","Placas de Vehiculo","Entrada"};
    private String visitas[]=new String[]{"Blanca Ramos; Calle Desteza; I9G56DTAGHHJ9; SS-46-9A;3/Diciembre/2017;11:11pm;null;null",
            "Cesar Rodriguez; Avenida Android 123; JA7FH3787R9CJ; G1-4H-8A;3/Diciembre/2017;11:15pm;3/Diciembre/2017;11:16pm",
                    "Cesar Rodriguez; Valle IHC; JA7FH3787R9CJ; G1-4H-8A;3/Diciembre/2017;11:15pm;3/Diciembre/2017;11:16pm",
                    "Jonathan Alcantara; Calle 123; JASCU7230UC89; 61-3L-HJS;3/Diciembre/2017;11:16pm;null;null",
                    "Emanuel Garcia; Calle 3; MXKLÑQOWID09D; 77-8J-00;3/Diciembre/2017;11:25pm;3/Diciembre/2017;11:26pm"};

    ListView tv;
    //MiBaseDatos MBD=new MiBaseDatos(getApplicationContext());

    ArrayList<String> listainformacion;
    ArrayList<String> listaUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visita);
        menu=(ImageButton)findViewById(R.id.Menu);
        buscar=(ImageButton)findViewById(R.id.Buscar);
        salir=(Button)findViewById(R.id.Salida);
        tv=(ListView)findViewById(R.id.tablavis);

        ArrayAdapter adaptador=new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,visitas);
        tv.setAdapter(adaptador);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menu=new Intent(view.getContext(), Menu.class);
                startActivityForResult(menu,0);
            }
        });



        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder falta=new AlertDialog.Builder(Visita.this);
                falta.setMessage("Quien saldra?:  ");
                final EditText et=new EditText(Visita.this);
                final TextView hor=new TextView(Visita.this);
                final TextView dia=new TextView(Visita.this);
                et.setGravity(Gravity.CENTER_HORIZONTAL);
                hor.setGravity(Gravity.CENTER_HORIZONTAL);
                dia.setGravity(Gravity.CENTER_HORIZONTAL);

                LinearLayout jaja=new LinearLayout(Visita.this);
                jaja.setOrientation(LinearLayout.VERTICAL);
                jaja.addView(et);
                jaja.addView(hor);
                jaja.addView(dia);
                falta.setView(jaja);

                Date d=new Date();
                SimpleDateFormat fec=new SimpleDateFormat("d'/'MMMM'/'yyyy");
                String fechaCompleta=fec.format(d);
                dia.setText(fechaCompleta);

                //Obtiene hora
                SimpleDateFormat hora=new SimpleDateFormat("h:mm a");
                String ho=hora.format(d);
                hor.setText(ho);


                falta.setPositiveButton("Registrar salida", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //MBD.modificarCONTACTO(et.getText().toString(),hor.getText().toString(),dia.getText().toString());
                        startActivity(new Intent(getBaseContext(),Visita.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        Toast.makeText(Visita.this, "Registro completo", Toast.LENGTH_SHORT).show();
                    }
                });
                falta.show();
            }
        });

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> b=new ArrayAdapter<String>(Visita.this, android.R.layout.simple_spinner_item,bus);
                final Spinner sp=new Spinner(Visita.this);
                sp.setGravity(Gravity.CENTER_HORIZONTAL);
                sp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                sp.setAdapter(b);
                AlertDialog.Builder falta=new AlertDialog.Builder(Visita.this);
                falta.setMessage("Seleccione la columna a buscar: ");
                falta.setView(sp);
                falta.setPositiveButton("OK", null);
                falta.show();
            }
        });
    }
}
