package com.example.equipoihc.safefractionation;

import android.content.Intent;
import android.database.SQLException;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class Residentes extends AppCompatActivity {

    ImageButton menu,buscar;
    String[]bu={"Nombre","Direccion","Telefono"};
    ListView ls;
    private String residentes[]=new String[]{"Ricardo Flores; Departamento 3; 87647594739",
            "Diego Ramos; Area 50; 884739202839",
                    "Alexa Rosas; Departamento AJ; 847498304739",
                    "Horacio Reyna; Casa 118; 78438393749"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_residentes);
        menu=(ImageButton)findViewById(R.id.Menu);
        buscar=(ImageButton)findViewById(R.id.Buscar);
        ls=(ListView)findViewById(R.id.l);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menu=new Intent(view.getContext(), Menu.class);
                startActivityForResult(menu,0);
            }
        });


        ArrayAdapter adaptador=new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,residentes);
        ls.setAdapter(adaptador);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> b=new ArrayAdapter<String>(Residentes.this, android.R.layout.simple_spinner_item,bu);
                final Spinner sp=new Spinner(Residentes.this);
                sp.setGravity(Gravity.CENTER_HORIZONTAL);
                sp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                sp.setAdapter(b);
                AlertDialog.Builder falta=new AlertDialog.Builder(Residentes.this);
                falta.setMessage("Seleccione la columna a buscar: ");
                falta.setView(sp);
                falta.setPositiveButton("OK", null);
                falta.show();
            }
        });

    }

    /*
    public java.sql.Connection conexionBD(){
        java.sql.Connection conexion=null;
        try{
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conexion= DriverManager.getConnection("jdbc:jtds:sqlserver://10.127.127.1:1433;databaseName=safefractionation;user=sa;password=e1657a815");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        return conexion;
    }
*/
}
