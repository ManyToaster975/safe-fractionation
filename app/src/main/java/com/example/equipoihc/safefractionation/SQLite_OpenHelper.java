package com.example.equipoihc.safefractionation;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Emanuel Garcia on 03/12/2017.
 */

public class SQLite_OpenHelper extends SQLiteOpenHelper {
    public SQLite_OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query="create table usuario(_ID integer primary key autoincrement, nombre text, correo text, hombre text, mujer text, edad text, contrasena text)";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void abrir(){
        this.getWritableDatabase();
    }
    public void cerrar(){
        this.close();
    }

    //Metodo para ingresar registros
    public void insertReg(String n, String c, String h, String m, String a, String p){
        ContentValues valor= new ContentValues();
        valor.put("nombre",n);
        valor.put("correo",c);
        valor.put("hombre",h);
        valor.put("mujer",m);
        valor.put("edad",a);
        valor.put("contrasena",p);
    }
}
