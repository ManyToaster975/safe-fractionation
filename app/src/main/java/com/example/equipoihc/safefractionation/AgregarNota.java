package com.example.equipoihc.safefractionation;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class AgregarNota extends AppCompatActivity {

    EditText notanueva;
    EditText contenido;
    Button crear;
    RadioButton baja,media,alta;
    //Variables para saber si hay una SD disponible y posible de escribir
    boolean sdDisponible=false;
    boolean sdAccesoEscritura=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_nota);
        notanueva=(EditText)findViewById(R.id.NombreArchivo);
        contenido=(EditText)findViewById(R.id.Contenido);
        crear=(Button)findViewById(R.id.Crear);
        baja=(RadioButton)findViewById(R.id.Baja);
        media=(RadioButton)findViewById(R.id.Media);
        alta=(RadioButton)findViewById(R.id.Alta);

        baja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contenido.setText("Prioridad: Baja: ");
            }
        });
        media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contenido.setText("Prioridad: Media: ");
            }
        });
        alta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contenido.setText("Prioridad: Alta: ");
            }
        });

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notanueva.getText().toString().isEmpty()){
                    Toast.makeText(AgregarNota.this,"Favor de seleccionar un nombre para el archivo",Toast.LENGTH_SHORT).show();
                }
                else{
                    try{
                        File route=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Notas");
                        if(route.mkdirs()){
                            File f=new File(route.getAbsolutePath(),notanueva.getText().toString());
                            OutputStreamWriter hoja=new OutputStreamWriter(new FileOutputStream(f));
                            hoja.write(contenido.getText().toString());
                            hoja.close();
                            Toast.makeText(AgregarNota.this, "El archivo se ha creado!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            File f=new File(route.getAbsolutePath(),notanueva.getText().toString());
                            OutputStreamWriter hoja=new OutputStreamWriter(new FileOutputStream(f));
                            hoja.write(contenido.getText().toString());
                            hoja.close();
                            Toast.makeText(AgregarNota.this, "El archivo se ha creado!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch(Exception ex){
                        Log.e("Datos","Error al escribir en hoja en memoria Externa");
                    }
                    finally {
                        finish();
                    }
                }
            }
        });
        //Comprueba el estado de la tarjeta Externa
        String estado= Environment.getExternalStorageState();
        if(estado.equals(Environment.MEDIA_MOUNTED)){
            sdDisponible=true;
            sdAccesoEscritura=true;
        }
        else{
            if(estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
                sdDisponible=true;
                sdAccesoEscritura=false;
            }
            else{
                sdDisponible=false;
                sdAccesoEscritura=false;
            }
        }

    }
}
